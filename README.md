## Ethereum Wallet Python Server


```Text    
    IP:  18.185.116.64
    Port: 8000
```


Server API in https://docs.google.com/document/d/17smSZmScRHWhjHJhi4xsE24IihKyo0SgbnO9ypJjU2c


Server Postman Collection [available here](https://gitlab.com/tecsynt_solutions/private-ethereum-backend-blockchain/blob/master/admin/EthereumWalletPy.postman_collection)

NOTE! You should set up ```environment.py``` before server start. Use ```environment.skel.py```